function formSupportCommissarValidate(config) {
    $("#form_com").validate({
        rules: config.rules,
        messages: config.messages,
        errorElement: "em",
        errorPlacement: function (error, element) {
            error.addClass("help-block");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parent().addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parent().addClass("has-success").removeClass("has-error");
        }
    });
}

$(function () {
    formSupportCommissarValidate(formSupportCommissarConfig);
    formSupportcommissar.handler = function () {
        formSupportCommissarValidate(formSupportCommissarConfig);
    };
});