<?

define("FAVORIT_AJAX", true);
define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$arResult = array();
ob_start();
$arResult['isOk'] = $APPLICATION->IncludeComponent("project.support:message.list", $_REQUEST["TEMPLATE_NAME"], Array(
    "IS_AJAX" => 'Y',
    "DELETE" => true,
    "PAGEN" => $_REQUEST["PAGEN_1"],
    "THEME_ID" => (int) $_REQUEST["THEME_ID"],
        ));
$arResult['content'] = ob_get_clean();
echo json_encode($arResult);
