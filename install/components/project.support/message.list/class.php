<?php

use Bitrix\Main\Loader,
    Project\Support\Model\ThemeTable,
    Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

class PortalMessageList extends CBitrixComponent {

    public function executeComponent() {
        if (CUser::IsAuthorized() and Loader::includeModule('project.support')) {
            $this->arResult['IS_AJAX'] = isset($this->arParams['IS_AJAX']) and $this->arParams['IS_AJAX'] == 'Y';
            $userId = cUser::GetID();
            if (!empty($this->arParams['DELETE']) and ! empty($this->arParams['THEME_ID'])) {
                $rsData = ThemeTable::getList(array(
                            'select' => array('ID'),
                            'filter' => array(
                                'THEME_ID' => $this->arParams['THEME_ID'],
                                'USER' => $userId,
                                'ACTIVE' => 1
                            ),
                ));
                $rsData = new CDBResult($rsData);
                if ($arItem = $rsData->Fetch()) {
                    ThemeTable::Update($arItem['ID'], array(
                        'ACTIVE' => 0
                    ));
                }
            }

            $rsData = ThemeTable::getList([
                        'select' => array(
                            'THEME_ID',
                        ),
                        'filter' => array(
                            'USER' => $userId,
                            'ACTIVE' => 1,
                            'BANNED' => 0,
                        )
            ]);
            $this->arResult['TOPIC'] = array();
            while ($arItem = $rsData->Fetch()) {
                $this->arResult['TOPIC'][] = $arItem['THEME_ID'];
            }

            if ($this->arResult['TOPIC']) {
                $this->arResult['PAGEN'] = empty($_GET['PAGEN_1']) ? 1 : $_GET['PAGEN_1'];
                $this->arResult['AJAX'] = $this->GetPath() . '/ajax.php';
                $this->arResult['SCRIPT'] = $this->GetPath() . '/script.js';
                $this->arResult['TEMPLATE_NAME'] = $this->GetTemplateName();
            }
            $this->includeComponentTemplate();
            return true;
        }
    }

}
