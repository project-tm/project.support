<?php

use Bitrix\Main\Loader,
    Project\Core\Utility,
    Igromafia\Game\User,
    Project\Support\Model\ThemeTable;

class PortalForumTopicMessage extends CBitrixComponent {

    public function executeComponent() {
        $this->arParams['THEME_ID'] = ceil($this->arParams['THEME_ID'] ?: 0);
        if (Loader::includeModule('project.support') and $this->arParams['THEME_ID']) {
            $this->arResult['IS_AJAX'] = isset($this->arParams['IS_AJAX']) and $this->arParams['IS_AJAX'] == 'Y';
            $this->arResult['IS_FILL'] = isset($this->arParams['IS_UPDATE']) ? (int) $this->arParams['IS_UPDATE'] : 1;
            $this->arResult['PAGE'] = $this->arParams['PAGE'] ?: 1;
            $this->arResult['LIMIT'] = 10;
            if ($this->arResult['IS_AJAX'] and $this->arParams['IS_UPDATE']) {
                $this->arResult['LIMIT'] *= $this->arParams['PAGE'];
                $this->arResult['PAGE'] = 1;
            }
            $rsData = ThemeTable::getList([
                        'select' => array(
                            'INTERLOCUTOR',
                            'THEME_ID',
                            'GAME_ID',
                            'THEME',
                        ),
                        'filter' => array(
                            'USER' => cUser::GetID(),
                            'THEME_ID' => $this->arParams['THEME_ID'],
                            'ACTIVE' => 1,
                            'BANNED' => 0,
                        )
            ]);
            if ($arItem = $rsData->Fetch()) {
                $this->arResult['THEME'] = $arItem['THEME'];
                $this->arResult['THEME_ID'] = $arItem['THEME_ID'];
                $this->arResult['INTERLOCUTOR'] = $arItem['GAME_ID'] ?: $arItem['INTERLOCUTOR'];
                if ($arItem['GAME_ID']) {
                    $this->arResult['GAME'] = Utility::useCache(array(__CLASS__, __FUNCTION__, 'GAME', $arItem['GAME_ID']), function() use($arItem) {
                                $arSelect = Array("ID", "NAME", "DETAIL_PAGE_URL");
                                $arFilter = Array("IBLOCK_ID" => \Igromafia\Game\Config::DZHO_IBLOCK, "ID" => $arItem['GAME_ID']);
                                $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
                                if ($arItem = $res->GetNext()) {
                                    return array(
                                        'NAME' => $arItem['NAME'],
                                        'URL' => $arItem['DETAIL_PAGE_URL'],
                                    );
                                }
                                return false;
                            });
                }

                $this->arResult['MESSAGE'] = array();
                $res = CForumMessage::GetList(
                                array("ID" => "DESC"), array("TOPIC_ID" => $arItem['THEME_ID'], 'APPROVED' => 'Y'), false, false, array(
                            "bDescPageNumbering" => false,
                            "nPageSize" => $this->arResult["LIMIT"],
                            "bShowAll" => false,
                            "iNumPage" => ($this->arResult['PAGE'] ?: false)
                ));
                $this->arResult['PAGE_COUNT'] = $res->NavPageCount;
                $this->arResult['PAGE_ITEM'] = $res->NavPageNomer;
                $this->arResult['PAGE_IS_NEXT'] = $this->arResult['PAGE_ITEM'] < $this->arResult['PAGE_COUNT'];

                $this->arResult['POST'] = $res->nSelectedCount;

                $parser = new forumTextParser(LANGUAGE_ID);
                $parser->imageWidth = $parser->imageHeight = (array_key_exists("IMAGE_SIZE", $arParams) ? $arParams["IMAGE_SIZE"] : 200);
                $parser->userPath = $arParams["URL_TEMPLATES_PROFILE_VIEW"];
                $parser->userNameTemplate = $arParams["NAME_TEMPLATE"];

                while ($arMessage = $res->GetNext()) {
                    $arMessage["~POST_MESSAGE_TEXT"] = (COption::GetOptionString("forum", "FILTER", "Y") == "Y" ? $arMessage["~POST_MESSAGE_FILTER"] : $arMessage["~POST_MESSAGE"]);
                    $arUser = User::getById($arMessage["AUTHOR_ID"]);
                    if ($arUser['ID'] == cUser::GetID()) {
                        $arUser['NAME'] = 'Вы';
                    }

                    $arMessage["POST_MESSAGE_TEXT"] = $parser->convert(
                            $arMessage["~POST_MESSAGE_TEXT"], array_merge($arAllow, array(
                        "SMILES" => ($arMessage["USE_SMILES"] == "Y" ? $arParams["ALLOW_SMILES"] : "N"),
                            )), "html"
                    );

                    $this->arResult['MESSAGE'][] = array(
                        'ID' => $arMessage["ID"],
                        'PARAM1' => $arMessage["PARAM1"],
                        'POST_MESSAGE_TEXT' => $arMessage["POST_MESSAGE_TEXT"],
                        'POST_DATE' => CForumFormat::DateFormat($arParams["DATE_TIME_FORMAT"], $arMessage["POST_TIMESTAMP"]),
                        'AUTHOR' => $arUser,
                    );
                }

                $this->arResult['PAGEN'] = empty($_GET['PAGEN_1']) ? 1 : $_GET['PAGEN_1'];
                $this->arResult['AJAX'] = $this->GetPath() . '/ajax.php';
                $this->arResult['SCRIPT'] = $this->GetPath() . '/script.js';
                $this->arResult['TEMPLATE_NAME'] = $this->GetTemplateName();
                $this->includeComponentTemplate();
            }
            return $this->arResult['PAGE_IS_NEXT'] ?: 0;
        }
    }

}
