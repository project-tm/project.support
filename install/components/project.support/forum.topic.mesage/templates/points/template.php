<? if ($arResult['IS_FILL']) { ?>
    <?
    $first = $arResult['MESSAGE'][0];
    $last = $arResult['MESSAGE'][1] ?: 0;
    unset($arResult['MESSAGE'][0], $arResult['MESSAGE'][1]);
    ?>
    <div class="remove-icon" data-id="<?= $arParams['THEME_ID'] ?>"></div>
    <div class="edit-game edit-game-points">
        <? if ($arResult["POST"] > 2) { ?>
            <span class="trigger">Все начисления (<?= $arResult["POST"] ?>)</span>
        <? } ?>
    </div>
    <div class="wrap-icon wrap-icon-message">
        <div class="icon"><?=$first['POINTS'] ?></div>
    </div>
    <div class="message-body">
        <div class="theme"><?= $arResult['THEME'] ?></div>
        <div class="content-block">
            <?= $first['POST_MESSAGE_TEXT'] ?>
        </div>
    </div>
    <div class="clear"></div>
    <div class="messages-block-wrapper">
        <? if ($last) { ?>
            <div class="comment-block comment-block-message">
                <div class="u-icon"><?=$last['POINTS'] ?></div>
                <div class="u-comment">
                    <?= $last['POST_MESSAGE_TEXT'] ?>
                </div>
                <div class="clear"></div>
            </div>
        <? } ?>
        <div class="messages-block-wrapper-last">
            <div class="messages-block-wrapper-comment">
            <? } ?>
            <? foreach ($arResult['MESSAGE'] as $arItem) { ?>
                <div class="comment-block comment-block-message">
                    <div class="u-icon"><?=$arItem['POINTS'] ?></div>
                    <div class="u-comment">
                        <?= $arItem['POST_MESSAGE_TEXT'] ?>
                    </div>
                    <div class="clear"></div>
                </div>
            <? } ?>
            <? if ($arResult['IS_FILL']) { ?>
            </div>
            <? if ($arResult['PAGE_IS_NEXT']) { ?>
                <div class="wrap_show-more">
                    <div class="show-more">Показать еще </div>
                </div>
            <? } ?>
        </div>
    </div>
<? } ?>
<? if (empty($arResult['IS_AJAX'])) { ?>
    <?
    $jsParams = array(
        "AJAX" => $arResult["AJAX"],
        "TEMPLATE_NAME" => $arResult["TEMPLATE_NAME"],
        "THEME_ID" => $arResult["THEME_ID"],
        "MORE" => '.show-more',
        "COMMENT" => '.messages-block-wrapper-comment',
        "CONTANER" => '.messages-block-' . $arResult['THEME_ID'],
    );
    ?>
    <script>
        portalForumMessageList[<?= $arResult['THEME_ID'] ?>] = new jsPortalForumMessageList(<?= CUtil::PhpToJSObject($jsParams, false, true) ?>);
    </script>
<? } ?>