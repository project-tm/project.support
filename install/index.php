<?php

use Bitrix\Main\Application,
    Bitrix\Main\ModuleManager,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader,
    Project\Support\Model\ThemeTable;

IncludeModuleLangFile(__FILE__);

class project_support extends CModule {

    public $MODULE_ID = 'project.support';

    function __construct() {
        $arModuleVersion = array();

        include(__DIR__ . '/version.php');

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('PROJECT_SUPPORT_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJECT_SUPPORT_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PROJECT_SUPPORT_PARTNER_NAME');
        $this->PARTNER_URI = '';
    }

    public function DoInstall() {
        ModuleManager::registerModule($this->MODULE_ID);
        Loader::includeModule($this->MODULE_ID);
        $this->InstallDB();
        $this->InstallEvent();
        $this->InstallFiles();
    }

    public function DoUninstall() {
        Loader::includeModule($this->MODULE_ID);
        $this->UnInstallEvent();
        $this->UnInstallFiles();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    public function InstallDB() {
        $this->dropTable();
        $this->GetConnection()->query("CREATE TABLE " . ThemeTable::getTableName() . " (
            ID INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
            ACTIVE INT,
            BANNED INT,
            USER INT,
            INTERLOCUTOR VARCHAR(255),
            FORUM_ID INT,
            THEME_ID INT,
            THEME_START INT,
            THEME VARCHAR(255),
            GAME_ID INT,
            NEW INT
        );");
    }

    public function dropTable() {
        $this->GetConnection()->query("DROP TABLE IF EXISTS " . ThemeTable::getTableName() . ";");
    }

    protected function GetConnection() {
        return Application::getInstance()->getConnection(ThemeTable::getConnectionName());
    }

    public function InstallEvent() {
        $eventManager = Bitrix\Main\EventManager::getInstance();
        $eventManager->registerEventHandler('forum', 'onAfterMessageAdd', $this->MODULE_ID, '\Project\Support\Event\Forum', 'onAfterMessageAdd');
    }

    public function UnInstallEvent() {
        $eventManager = Bitrix\Main\EventManager::getInstance();
        $eventManager->unRegisterEventHandler('forum', 'onAfterMessageAdd', $this->MODULE_ID, '\Project\Support\Event\Forum', 'onAfterMessageAdd');
    }

    /*
     * InstallFiles
     */

    public function InstallFiles($arParams = array()) {
        CopyDirFiles($_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/install/components/project.support/', $_SERVER['DOCUMENT_ROOT'] . '/local/components/project.support/' . $this->MODULE_ID . '/', true, true);
    }

    public function UnInstallFiles() {
        DeleteDirFiles($_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/install/components/project.support/', $_SERVER['DOCUMENT_ROOT'] . '/local/components/project.support/' . $this->MODULE_ID . '/'); //css
    }

}
