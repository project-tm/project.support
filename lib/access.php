<?php

namespace Project\Support;

class Access {

    public function is() {
        global $USER;
        return $USER->IsAdmin() or User::isModerator();
    }

}
