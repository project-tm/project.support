<?php

namespace Project\Support;

use Project\Core\Utility,
    Project\Support\Model\ThemeTable,
    CUser,
    CDBResult,
    Exception,
    CForumTopic;

class Forum {

    static public function getId($interlocutor) {
        switch ($interlocutor) {
            case 'commissar':
                return Config::FORUM_COMMISSAR;
            case 'points':
                return Config::FORUM_POINT;
            case 'support':
                return Config::FORUM_SUPPORT;
            default:
                return Config::FORUM_MESSAGE;
        }
    }

    static public function getTopicId($interlocutor, $arData) {
        static $arCache = array();
        $userId = CUser::GetID();
        $gameId = $arData['GAME_ID'] ?: 0;

        if (is_string($arData['THEME'])) {
            $arData['THEME'] = array(
                'USER' => $arData['THEME'],
                'SELLER' => $arData['THEME'],
                'FORUM' => $arData['THEME'],
            );
        }
        if (empty($arCache[$interlocutor])) {
            $arCache[$interlocutor] = Utility::useCache(array(__CLASS__, __FUNCTION__, $userId, $interlocutor, $gameId), function() use($userId, $interlocutor, $gameId, $arData) {
                        $arFilter = array(
                            'USER' => $userId,
                        );
                        if ($gameId) {
                            $arFilter['GAME_ID'] = $gameId;
                        } else {
                            $arFilter['INTERLOCUTOR'] = $interlocutor;
                        }
                        $rsData = ThemeTable::getList(array(
                                    "select" => array('ID', 'THEME_ID'),
                                    "filter" => $arFilter
                        ));
                        $rsData = new CDBResult($rsData);
                        if ($arItem = $rsData->Fetch()) {
                            $isSearchIb = $arItem['ID'];
                            if (CForumTopic::GetByID($arItem['THEME_ID'])) {
                                return $arItem['THEME_ID'];
                            }
                        }
                        if (empty($themeId)) {
                            $listId = [$interlocutor, $userId];
                            if($gameId) {
                                $listId[] = 'game' . $gameId;
                            }
                            sort($listId);
                            $xmlId = implode(':', $listId);
                            $arTheme = CForumTopic::GetList(array(), array('FORUM_ID' => self::getId($interlocutor), 'XML_ID' => $xmlId))->Fetch();
                            if ($arTheme) {
                                $themeId = $arTheme['ID'];
                            } else {
                                $themeForum = array();
                                $format = "#NAME# #LAST_NAME# (#EMAIL#)[#ID#]";
                                foreach ($listId as $value) {
                                    if (is_numeric($value)) {
                                        $arUser = cUser::getbyid($value)->Fetch();
                                        $themeForum[] = CUser::FormatName($format, $arUser, false, false);
                                    }
                                }
                                $arFields = array(
                                    'TITLE' => $arData['THEME']['FORUM'],
                                    'DESCRIPTION' => implode(', ', $themeForum),
                                    'XML_ID' => $xmlId,
                                    'FORUM_ID' => self::getId($interlocutor),
                                    'USER_START_ID' => $userId,
                                    'USER_START_NAME' => CUser::GetLogin(),
                                    'LAST_POSTER_NAME' => CUser::GetLogin(),
                                );
                                pre($arFields);
                                $themeId = CForumTopic::Add($arFields);
                            }

                            $arBxData = array(
                                'USER' => $userId,
                                'INTERLOCUTOR' => $interlocutor,
                                'GAME_ID' => $gameId,
                                'THEME_ID' => $themeId,
                                'THEME' => $arData['THEME']['USER'],
                            );
                            if ($isSearchIb) {
                                ThemeTable::update($isSearchIb, $arBxData);
                            } else {
                                $result = ThemeTable::add($arBxData);
                                if (!$result->isSuccess()) {
                                    throw new Exception('Ошибка создания чата');
                                }
                            }
                            if (is_numeric($interlocutor)) {
                                $arBxData = array(
                                    'USER' => $interlocutor,
                                    'INTERLOCUTOR' => $userId,
                                    'GAME_ID' => $gameId,
                                    'THEME_ID' => $themeId,
                                    'THEME' => $arData['THEME']['SELLER'],
                                );
                                $rsData = ThemeTable::getList(array(
                                            "select" => array('ID'),
                                            "filter" => array(
                                                'USER' => $arBxData['USER'],
                                                'INTERLOCUTOR' => $arBxData['INTERLOCUTOR'],
                                                'GAME_ID' => $arData['GAME_ID'] ?: 0
                                            )
                                ));
                                $rsData = new CDBResult($rsData);
                                if ($arItem = $rsData->Fetch()) {
                                    ThemeTable::update($arItem['ID'], $arBxData);
                                } else {
                                    $result = ThemeTable::add($arBxData);
                                    if (!$result->isSuccess()) {
                                        throw new Exception('Ошибка создания чата');
                                    }
                                }
                            }
                            return $themeId;
                        }
                    });
        }
        return $arCache[$interlocutor];
    }

    static public function getListTopic($isOpen = true) {
        $userId = CUser::GetID();
        return Utility::useCache(array(__CLASS__, __FUNCTION__, $userId), function() use($userId) {
                    $rsData = ThemeTable::getList(array(
                                "select" => array('INTERLOCUTOR', 'THEME_ID', 'THEME'),
                                "filter" => array('USER' => $userId)
                    ));
                    $rsData = new CDBResult($rsData);
                    $arResult = array();
                    while ($arItem = $rsData->Fetch()) {
                        $arItem['MAX'] = $arItem['UF_LAST'];
                        $arData[$arItem['UF_CHAT']] = $arItem;
                        $arFilter['THEME'][] = $arItem;
                    }
                    return array(
                        'user' => User::getDataById(User::GetId()),
                        'theme' => (count($arResult) == 1 and User::isClient()) ? false : $arResult,
                        'interlocutorId' => $arResult[0]['interlocutor'],
                        'count' => $count,
                        'format' => Utility::FormatNewMessage($count)
                    );
                }, 5);
    }

}
