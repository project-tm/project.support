<?php

namespace Project\Support;

class Config {

    const FORUM_MESSAGE = 3;
    const FORUM_SUPPORT = 2;
    const FORUM_COMMISSAR = 4;
    const FORUM_POINT = 5;
    const FORUMS = [self::FORM_COMMISSAR_ID, self::FORUM_SUPPORT, self::FORUM_COMMISSAR, self::FORUM_POINT];
    const FORM_SUPPORT_ID = 4;
    const FORM_COMMISSAR_ID = 3;
    const MODERATOR = [7];

}
