<?

namespace Project\Support\Event;

use CDBResult,
    Project\Support\Model\ThemeTable;

class Forum {

    public static function onAfterMessageAdd($ID, $arFileds) {
        $rsData = ThemeTable::getList(array(
                    "select" => array('ID'),
                    "filter" => array(
                        'ACTIVE' => 0,
                        'THEME_ID' => $arFileds['TOPIC_ID']
                    )
        ));
        $rsData = new CDBResult($rsData);
        while ($arItem = $rsData->Fetch()) {
            ThemeTable::update($arItem['ID'], array(
                'ACTIVE' => 1
            ));
        }
    }

}
