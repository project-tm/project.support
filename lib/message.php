<?php

namespace Project\Support;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main\Localization\Loc,
    Igromafia\Game\User,
    Exception,
    CForumMessage;

Loc::loadMessages(__FILE__);

/**
 * Модель новостей.
 */
class Message {

    static $interlocutorList = array();

    public static function addChat($interlocutor) {
        self::$interlocutorList[] = $interlocutor;
    }

    public static function getChat() {
        return self::$interlocutorList;
    }

    public static function add($interlocutor, $arData) {
        global $APPLICATION;
        $arUser = User::getInfo();
        $arFields = Array(
            "POST_MESSAGE" => $arData['MESSAGE'],
            "PARAM1" => $arData['PARAM1'] ?: '',
            "USE_SMILES" => "Y",
            "APPROVED" => 'Y',
            "AUTHOR_NAME" => $arUser['NAME'],
            "AUTHOR_ID" => $arUser['ID'],
            "FORUM_ID" => Forum::getId($interlocutor),
            "TOPIC_ID" => Forum::getTopicId($interlocutor, $arData),
            "AUTHOR_IP" => empty($_SESSION['SESS_IP']) ? '' : $_SESSION['SESS_IP'],
            "NEW_TOPIC" => "N",
        );

        if ($lastId = CForumMessage::Add($arFields)) {
            return '//' . $_SERVER['SERVER_NAME'] . '/support/messages/forum' . Forum::getId($interlocutor) . '/message' . $lastId . '/';
        } else {
            if ($ex = $APPLICATION->GetException()) {
                throw new Exception(strip_tags($ex->GetString()));
            }
            throw new Exception('Ошибка создания чата');
        }
        return false;
    }

}
