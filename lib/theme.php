<?php

namespace Project\Support;

class Theme {

    static public function getHeader($interlocutor) {
        switch ($interlocutor) {
            case 'commissar':
                return 'Сообщение комиссару';
            case 'support':
                return 'Сообщение поддержке';
            default:
                return 'Обратная связь';
        }
    }

}
