<?php

namespace Project\Support;

class Form extends \Project\Core\Form {

    static public function getId($interlocutor) {
        switch ($interlocutor) {
            case 'commissar':
                return Config::FORM_COMMISSAR_ID;
            case 'support':
                return Config::FORM_SUPPORT_ID;
            default:
                return false;
        }
    }

    static public function add($interlocutor, $arData) {
        if (self::getId($interlocutor)) {
            return parent::add(self::getId($interlocutor), $arData);
        }
        return true;
    }

}
